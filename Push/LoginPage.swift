//
//  LoginPage.swift
//  Push
//
//  Created by Malik Bunton on 2/1/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//

import UIKit
import Parse
import AVFoundation
import MediaPlayer

class LoginPage: UIViewController, UITextFieldDelegate, MPMediaPickerControllerDelegate {

    @IBOutlet var usernameSignUp: UITextField!
    @IBOutlet var passwordSignUp: UITextField!
    @IBOutlet var confirmPassword: UITextField!
    @IBOutlet var usernameLogin: UITextField!
    @IBOutlet var passwordLogin: UITextField!
    
    @IBOutlet var errorSignUpLabel: UILabel!
    @IBOutlet var errorLoginLabel: UILabel!
    
    var spinner: UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBAction func signUpButton(_ sender: AnyObject) {
        //This line is temporary
        self.performSegue(withIdentifier: "login/signupSeg", sender: self)
        /*
        if usernameSignUp.text == "" || passwordSignUp.text == ""{
            errorSignUpLabel.text = "Please type something in the box"
        }
        else{
            errorSignUpLabel.text = ""
            //Make a spinner
            spinner = UIActivityIndicatorView(frame: CGRect(x: 0,y: 0,width: 50,height: 50))
            spinner.center = self.view.center
            spinner.hidesWhenStopped = true
            spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            view.addSubview(spinner)
            spinner.startAnimating()
            UIApplication.shared.beginIgnoringInteractionEvents()
            
            //create a user
            let user = PFUser()
            
            //checks if passwords match
            if passwordSignUp.text != confirmPassword.text{
                self.errorSignUpLabel.text = "passwords do not match"
                self.spinner.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            else{
                //sends password and username
                user.username = usernameSignUp.text
                user.password = passwordSignUp.text
                
                var errorMessage = ""
                
                user.signUpInBackground(block: { (success, error) -> Void in
                    self.spinner.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    if error == nil{
                        //Successful sign up!!
                        //MOVE TO THE NEXT PAGE
                        
                        self.performSegue(withIdentifier: "login/signupSeg", sender: self)
                    }
                        
                    else{
                        //if theres an error in the sign up then say it
                        if let errorString = error.  ["error"] as? String{
                            errorMessage =  errorString
                            self.errorSignUpLabel.text = errorMessage
                        }
                        else{
                            self.errorSignUpLabel.text = "Failed Sign Up"
                        }
                    }
        
                })
            }
        } */
    }
    
    @IBAction func loginButton(_ sender: AnyObject) {
        //This line is temporary
        self.performSegue(withIdentifier: "login/signupSeg", sender: self) /*
        spinner.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        if usernameLogin.text == "" || passwordLogin.text == ""{
            errorLoginLabel.text = "Please type something in the box"
            self.spinner.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
        else{
            PFUser.logInWithUsername(inBackground: usernameLogin.text!, password:passwordLogin.text!) {
                (user, error) -> Void in
                if user != nil {
                    self.spinner.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    self.errorLoginLabel.text = "Welcome"
                    //MOVE TO THE NEXT PAGE
                    self.performSegue(withIdentifier: "login/signupSeg", sender: self)
                    // Do stuff after successful login.
                } else {
                    if let errorMessage = error!.userInfo["error"] as? String{
                        self.errorLoginLabel.text = errorMessage
                        self.spinner.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                    }
                    else{
                        self.spinner.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        self.errorLoginLabel.text = "Something is wrong try again later"
                    }
                }
            }
        } */
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.usernameLogin.delegate = self
        self.passwordLogin.delegate = self
        self.passwordSignUp.delegate = self
        self.confirmPassword.delegate = self
        self.usernameSignUp.delegate = self
        
        
         /*
        let picker = MPMediaPickerController()
        picker.mediaTypes.contains(MPContentItem) = true
        picker.delegate = self
        self.presentViewController(picker, animated: true, completion: nil)
        
       
        var player = MPMusicPlayerController()
        player.setQueueWithStoreIDs(["3364278122281073904"])
        player.play() */
        
       
    }
    //If Sreen Tapped Keyboard Goes Away
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField:UITextField)-> Bool{
        textField.resignFirstResponder()
        return true
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    /*
    func mediaPicker(mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        self.dismissViewControllerAnimated(true, completion: {
            for i:MPMediaItem in mediaItemCollection.items {
                print(i.id)
            }
        })
    }
    
    func mediaPickerDidCancel(mediaPicker: MPMediaPickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }*/
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    /*var storekitView = SKStoreProductViewController()
     storekitView.loadProductWithParameters([SKStoreProductParameterITunesItemIdentifier:1129952240], completionBlock: nil)
     storekitView.delegate = self */
    
    /*MPMediaLibrary.defaultMediaLibrary().addItemWithProductID("578465777", completionHandler: {(entity, error) in
     
     }) */
    //LITTTTTT
    /* var player = MPMusicPlayerController()
     player.setQueueWithStoreIDs(["578465777"])
     player.play()
     */
    
    //self.presentViewController(storekitView, animated: true, completion: nil)
    //gets the aurtoriziation status of the users library
    //print(SKCloudServiceController.authorizationStatus().rawValue)
    //print(MPMediaLibrary.authorizationStatus().rawValue)
    
    //How to ask the user
    //SKCloudServiceController.requestAuthorization()
    
    /*var closuer = {(service:SKCloudServiceAuthorizationStatus, error:NSError?) -> Void in
     print(service.rawValue)
     }*/
    /*
     SKCloudServiceController().requestCapabilitiesWithCompletionHandler({(status,error) in
     print(status)
     print("error\(error)")
     }) */
    
    /*SKCloudServiceController().requestStorefrontIdentifierWithCompletionHandler({(identifier, error) in
     print(identifier)
     print(error)
     }) */
    
    
    //MPMediaLibrary.defaultMediaLibrary().addItemWithProductID(, completionHandler: )//

}
