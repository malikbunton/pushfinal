//
//  MusicTableCell.swift
//  Push
//
//  Created by Malik Bunton on 2/16/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//

import UIKit

class MusicTableCell: UITableViewCell {
    
    @IBOutlet var albumArt: UIImageView!
    
    @IBOutlet var trackTitle: UILabel!

    @IBOutlet var trackArtist: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
