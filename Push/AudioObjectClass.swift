//
//  AudioObjectClass.swift
//  Push
//
//  Created by Malik Bunton on 6/20/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class AudioObjectClass {
    var artist: NSString?
    var audioFile: AnyObject?
    var image: UIImage?
    var mediaType: songType?
    var sender: NSString?
    var title: NSString?
    var id : String?
    var uuid: String?
    
    func initWithProperties(Title title: NSString,
                      File audioFile: AnyObject,
                      Type mediaType: songType,
                       Artist artist: NSString,
                         Image image: UIImage,
                       Sender sender: NSString,
                               ID id: String){
        
        self.audioFile = audioFile
        self.mediaType = mediaType
        self.sender = sender
        self.title = title
        self.artist = artist
        self.image = image
        self.id = id
        
    }
    
    convenience init(snapshot: FIRDataSnapshot){
        self.init()
        let snapshotValue = snapshot.value as! [String: AnyObject]
        self.audioFile = snapshotValue["kDataHandlerKeyFor_DataType"] as! String as AnyObject?
        self.mediaType = snapshotValue["kDataHandlerAudioObjectTypeOptions_appleMusic"] as? songType
        self.title = snapshotValue["kDataHandlerKeyFor_AudioObjectTitle"] as! String as NSString?
        self.artist = snapshotValue["kDataHandlerKeyFor_AudioObjectArtist"] as! String as NSString?
        let artEncodedString: String =
            (snapshotValue[kDataHandlerKeyFor.AudioObjectArtwork] as? String)!
        let artData: Data = Data.init(base64Encoded: artEncodedString,
                                      options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
        self.image =  UIImage.init(data: artData as Data)!
//        if let artworkUrl = URL(string: artEncodedString){
//        do{
//            let artData:Data = try Data.init(contentsOf: artworkUrl)
//                self.image =  UIImage.init(data: artData as Data)!
//            } catch {
//                print("Cannot Init artwork")
//            }
//        }

        self.id = snapshotValue["kDataHandlerKeyFor_AudioObjectID"] as! String
        self.uuid = snapshotValue["kDataHandlerKeyFor_AudioObjectUUID"] as! String

        
    }

    
}
