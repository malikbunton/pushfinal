//
//  DatabaseManager.swift
//  Push
//
//  Created by Ibrahim Conteh on 9/18/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//

import UIKit
import AWSDynamoDB

let AWSSampleDynamoDBTableName = "DynamoDB-OM-SwiftSample"

class DatabaseManager: NSObject {
    
    class func describeTable() -> AWSTask<AnyObject> {
        let dynamoDB = AWSDynamoDB.default()
        
        // See if the test table exists.
        let describeTableInput = AWSDynamoDBDescribeTableInput()
        describeTableInput?.tableName = AWSSampleDynamoDBTableName
        return dynamoDB.describeTable(describeTableInput!) as! AWSTask<AnyObject>
    }
    
    class func createTable() -> AWSTask<AnyObject> {
        let dynamoDB = AWSDynamoDB.default()
        
        let hashKeyAttributeDefinition = AWSDynamoDBAttributeDefinition()
        hashKeyAttributeDefinition?.attributeName = "Name of playlist"
        hashKeyAttributeDefinition?.attributeType = AWSDynamoDBScalarAttributeType.S
        
        let hashKeySchemaElement = AWSDynamoDBKeySchemaElement()
        hashKeySchemaElement?.attributeName = "Name of playlist"
        hashKeySchemaElement?.keyType = AWSDynamoDBKeyType.hash
        
        let rangeKeyAttributeDefinition = AWSDynamoDBAttributeDefinition()
        rangeKeyAttributeDefinition?.attributeName = "Username"
        rangeKeyAttributeDefinition?.attributeType = AWSDynamoDBScalarAttributeType.S
        
        let rangeKeySchemaElement = AWSDynamoDBKeySchemaElement()
        rangeKeySchemaElement?.attributeName = "Username"
        rangeKeySchemaElement?.keyType = AWSDynamoDBKeyType.range
        
        let createTableInput = AWSDynamoDBCreateTableInput()
        createTableInput?.tableName = AWSSampleDynamoDBTableName;
        createTableInput?.attributeDefinitions = [hashKeyAttributeDefinition!, rangeKeyAttributeDefinition!]
        createTableInput?.keySchema = [hashKeySchemaElement!, rangeKeySchemaElement!]
        
        
        return dynamoDB.createTable(createTableInput!).continue(successBlock: { task -> AnyObject? in
            var localTask = task
            
            if ((localTask.result) != nil) {
                // Wait for up to 4 minutes until the table becomes ACTIVE.
                
                let describeTableInput = AWSDynamoDBDescribeTableInput()
                describeTableInput?.tableName = AWSSampleDynamoDBTableName;
                localTask = dynamoDB.describeTable(describeTableInput!) as! (AWSTask<AWSDynamoDBCreateTableOutput>)
                
                for _ in 0...15 {
                    localTask = localTask.continue(successBlock: { task -> AnyObject? in
                        let describeTableOutput:AWSDynamoDBDescribeTableOutput = task.result as! AWSDynamoDBDescribeTableOutput
                        let tableStatus = describeTableOutput.table!.tableStatus
                        if tableStatus == AWSDynamoDBTableStatus.active {
                            return task
                        }
                        
                        sleep(15)
                        return dynamoDB .describeTable(describeTableInput!)
                    }) as! (AWSTask<AWSDynamoDBCreateTableOutput>)
                }
            }
            
            return localTask
        })
    }
}

class DDBTableRow :AWSDynamoDBObjectModel ,AWSDynamoDBModeling  {
    
    var NameOfPlaylist:String?
    var ContributorName:String?
    var Username:String?
    var Song:AudioObjectClass?
    
    
    //should be ignored according to ignoreAttributes
    var internalName:String?
    var internalState:NSNumber?
    
    class func dynamoDBTableName() -> String {
        return AWSSampleDynamoDBTableName
    }
    
    class func hashKeyAttribute() -> String {
        return "NameOfPlaylist"
    }
    
    class func rangeKeyAttribute() -> String {
        return "Username"
    }
    
    class func ignoreAttributes() -> [String] {
        return ["internalName", "internalState"]
    }
}
