//
//  UserCollectionReusableView.swift
//  Push
//
//  Created by Nashawn Chery on 1/9/18.
//  Copyright © 2018 Malik Bunton. All rights reserved.
//

import UIKit
import FirebaseDatabase

class UserCollectionReusableView: UICollectionReusableView {
    
    var databaseRef: FIRDatabaseReference!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .black
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    let usernameLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 255/255.0, green: 120/255.0, blue: 0/255.0, alpha: 1)
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        return label
    }()
    
    let dividerLineView: UIView = {
        let line = UIView()
        line.backgroundColor = UIColor.init(white: 0.4, alpha: 0.4)
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
    
    let profilePic: UIImageView = {
        let imageView = UIImageView()
        imageView.image = nil
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 37.5
        imageView.layer.borderWidth = 1.0
        imageView.layer.borderColor = UIColor.black.cgColor
        imageView.layer.masksToBounds = true
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let playlistsLabel: UILabel = {
        let label = UILabel()
        label.text = "Playlists: 9000"
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.white
        return label
    }()
    
    override func layoutSubviews() {
        addSubview(profilePic)
        addSubview(usernameLabel)
        addSubview(dividerLineView)
        addSubview(playlistsLabel)
        
        backgroundColor = .black
        profilePic.autoSetDimension(.height, toSize: 75)
        profilePic.autoSetDimension(.width, toSize: 75)
        profilePic.autoPinEdge(toSuperviewEdge: .top, withInset: 10)
        profilePic.autoPinEdge(toSuperviewEdge: .left, withInset: 10)
        
        usernameLabel.autoSetDimension(.width, toSize: frame.width)
        usernameLabel.autoPinEdge(.left, to: .right, of: profilePic, withOffset: 10)
        usernameLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 15)
        
        playlistsLabel.autoSetDimension(.width, toSize: frame.width)
        playlistsLabel.autoPinEdge(.left, to: .right, of: profilePic, withOffset: 10)
        playlistsLabel.autoPinEdge(.top, to: .bottom, of: usernameLabel, withOffset: 5)
        
        dividerLineView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 10)
        dividerLineView.autoPinEdge(toSuperviewEdge: .left, withInset: 14)
        dividerLineView.autoPinEdge(toSuperviewEdge: .right, withInset: 14)
        dividerLineView.autoSetDimension(.height, toSize: 1)
        dividerLineView.autoSetDimension(.width, toSize: frame.width)
    }
        
}
