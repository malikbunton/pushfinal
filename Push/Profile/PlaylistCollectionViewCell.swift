//
//  PlaylistCollectionViewCell.swift
//  Push
//
//  Created by Nashawn Chery on 1/9/18.
//  Copyright © 2018 Malik Bunton. All rights reserved.
//

import UIKit

class PlaylistCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        return label
    }()
    
    var creatorLabel: UILabel = {
        let label = UILabel()
        label.text = "NashawnCh"
        label.textColor = UIColor(red: 255/255.0, green: 120/255.0, blue: 0/255.0, alpha: 1)
        label.font = UIFont.italicSystemFont(ofSize: 12.0)
        return label
    }()
    
    
    let imageView: UIImageView  = {
        let imageView = UIImageView()
        return imageView
    }()
    
    func setupViews() {
        addSubview(imageView)
        addSubview(creatorLabel)
        addSubview(titleLabel)
        
        titleLabel.textColor =  UIColor(red: 255/255.0, green: 120/255.0, blue: 0/255.0, alpha: 1)
        titleLabel.autoSetDimension(.width, toSize: frame.width)
        titleLabel.autoPinEdge(.bottom, to: .bottom, of: self.imageView, withOffset: 20)
        titleLabel.autoPinEdge(.left, to: .left, of: self.imageView, withOffset: 5)
        
        /* Setup the creator label */
        creatorLabel.textColor = UIColor.gray
        creatorLabel.autoSetDimension(.width, toSize: frame.width)
        creatorLabel.autoPinEdge(.top, to: .bottom, of: titleLabel, withOffset: 1)
        creatorLabel.autoPinEdge(.left, to: .left, of: titleLabel)
        
        imageView.autoSetDimensions(to: CGSize(width: frame.width, height: frame.height))
    }
}
