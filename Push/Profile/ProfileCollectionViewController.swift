//
//  ProfileCollectionViewController.swift
//  Push
//
//  Created by Nashawn Chery on 1/9/18.
//  Copyright © 2018 Malik Bunton. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

private let reuseIdentifier = "Cell"

class ProfileCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var cellSpacing: CGFloat = 15
    var appDel: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var ref: FIRDatabaseReference!
    var storage: FIRStorage!
    var storageRef: FIRStorageReference!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        collectionView?.backgroundColor = .black
        
        self.collectionView!.register(PlaylistCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView!.register(UserCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView")

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkUpdates()
    }
    
    func checkUpdates() {
        if let user = FIRAuth.auth()?.currentUser?.uid {
            let ref = FIRDatabase.database().reference()
            
            ref.child("users").child(user).child("playlists").observe(.value, with: { snapshot in
                print(user)
                if snapshot.exists(){
                    var playlistCollection : [PlaylistCollection] = []
                    for item in snapshot.children{
                        let playlist = PlaylistCollection(snapshot: item as! FIRDataSnapshot)
                        print("Playlists")
                        playlistCollection.append(playlist)
                    }
                    playlistCollections = playlistCollection
                    self.collectionView!.reloadData()
                }
            })
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return playlistCollections.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let playlists = playlistCollections[indexPath.row]
        
        self.ref = FIRDatabase.database().reference()
        
        let alert = UIAlertController(title: "Would you like to Re-Host this Playlist?", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
           self.ref.child("playlists").child(playlists.playListTitle).observeSingleEvent(of: .value, with: {
                    snapshot in
                    if snapshot.exists(){
                        self.appDel.hostPlaylistName = playlists.playListTitle
                        self.tabBarController?.selectedIndex = 1
                    } else{
                        let unfoundPlaylistAlert = UIAlertController(title: "Cant locate playlist, Try Again", message: "", preferredStyle: .alert)
                        unfoundPlaylistAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                            return
                        }))
                        self.present(unfoundPlaylistAlert, animated: true, completion: nil)
                    }
                })
            }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {(action) -> Void in
        }))
        self.present(alert, animated: true, completion: nil)
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PlaylistCollectionViewCell
        
        let playlists = playlistCollections[indexPath.row]
        
        if let newTitle = playlists.playListTitle {
            cell.titleLabel.text = newTitle as String
        } else{ cell.titleLabel.text = nil }
        if let newImage = playlists.image {
            cell.imageView.image = newImage
        } else{ cell.imageView.image = nil }
        
    
        // Configure the cell
    
        return cell
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }

    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerView", for: indexPath) as! UserCollectionReusableView

            let singleTap = UITapGestureRecognizer(target: self, action: #selector(ProfileCollectionViewController.changeProfileImage))
            headerView.profilePic.addGestureRecognizer(singleTap)
            
            if let user = FIRAuth.auth()?.currentUser {
                    self.ref = FIRDatabase.database().reference()
                    self.storage = FIRStorage.storage()
                self.ref.child("users").child(user.uid).child("userInfo").observeSingleEvent(of: .value, with: { (snapshot) in
                    let value = snapshot.value as? NSDictionary
                    let username = value!["username"] as! String
                    headerView.usernameLabel.text = username
                    
                    
                    if (value!["photoUrl"] as! String) != "nil"{
                        let imageUrl = String(describing: user.photoURL!)
                        self.storage.reference(forURL: imageUrl).data(withMaxSize: 1 * 2048 * 2048) { (data, error) in
                            if let error = error{
                                print(error.localizedDescription)
                            }else{
                                if let data = data{
                                    let image = UIImage(data: data as Data)
                                    headerView.profilePic.image = image
                                }
                            }
                        }
                    }
                })
            }
            
            return headerView
        default:
            assert(false, "Unexpected element kind")
        }
    }
    
    func changeProfileImage(){
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        
        let alert = UIAlertController(title: "Choose Image", message: "", preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        }
        
        let photoAlbumAction = UIAlertAction(title: "Album library", style: .default) { (action) in
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
        }
        
        let savedPhotosAction = UIAlertAction(title: "Saved Phots", style: .default) { (action) in
            picker.sourceType = .savedPhotosAlbum
            self.present(picker, animated: true, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alert.addAction(cameraAction)
        alert.addAction(photoAlbumAction)
        alert.addAction(savedPhotosAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.dismiss(animated: true, completion: nil)
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        let data = UIImageJPEGRepresentation(image!, 0.8)
        if let user = FIRAuth.auth()?.currentUser {
            let imagePath = "profileImage\(user.uid)/userpic.jpg"
            
            self.storageRef = FIRStorage.storage().reference()
            let imageRef = storageRef.child(imagePath)
            
            let metaData = FIRStorageMetadata()
            metaData.contentType = "image/jpeg"
            imageRef.put(data as! Data, metadata: metaData) { (metaData, error) in
                if error == nil{
                    let changeRequest = user.profileChangeRequest()
                    changeRequest.displayName = user.displayName
                    changeRequest.photoURL = metaData!.downloadURL()!
                    changeRequest.commitChanges(completion: { (error) in
                        if error == nil{
                            self.ref = FIRDatabase.database().reference()
                            let userRef = self.ref.child("users").child(user.uid).child("userInfo").child("photoUrl")
            
                            userRef.setValue(String(describing: user.photoURL!))
                            self.collectionView?.reloadData()
                        }
                    })
                }
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (UIScreen.main.bounds.size.width - 3  * cellSpacing) / 2
        let height = width
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, cellSpacing, 0, cellSpacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }

}
