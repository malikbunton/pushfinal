//
//  UserQueueViewController.swift
//  Push
//
//  Created by Malik Bunton on 2/27/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//

import UIKit
import MultipeerConnectivity
import MediaPlayer
import Firebase

class UserQueueViewController: UIViewController, UITableViewDataSource,  UITableViewDelegate{
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SimpleCellClassTableViewCell = queueTable.dequeueReusableCell(withIdentifier: "SimpleCell", for: indexPath as IndexPath) as! SimpleCellClassTableViewCell
        
        let audioObject = audioObjects[indexPath.row]
        
        if let newTitle = audioObject.title {
            cell.songTitle.text = newTitle as String
        } else{ cell.songTitle.text = nil }
        if let newArtist = audioObject.artist {
            cell.artist.text = newArtist as String
        } else{ cell.artist.text = nil }
        if let newImage = audioObject.image {
            cell.picture.image = newImage
        } else{ cell.picture.image = nil }
        if let newSender = audioObject.sender {
            cell.sender.text = newSender as String
        }else{ cell.sender.text = nil }
        
        return cell
    }

    
    @IBOutlet var queueTable: UITableView!
    @IBOutlet var navigationBar: UINavigationBar!
    var appDel: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var cellID = "queueCell"
    var indexOfCurrentSong = 0
    var query: MPMediaQuery = MPMediaQuery.songs()
    var ref: FIRDatabaseReference!
    var user = FIRAuth.auth()?.currentUser
    let defaults = UserDefaults.standard
    
    // MARK: UI Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        queueTable.dataSource = self
        queueTable.delegate = self
        queueTable.tableFooterView = UIView()
        
        let nib = UINib(nibName: "CellDesignIpod", bundle: nil)
        queueTable.register(nib, forCellReuseIdentifier: "SimpleCell")
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kReloadJoinNotificationName), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadTable), name: NSNotification.Name(rawValue: kReloadJoinNotificationName), object: nil)
        
        self.ref = FIRDatabase.database().reference()
        //If the entered playlist name exists, then it will set it equal to playlistName and use it to sync to the database

//        if let playlistName = self.defaults.string(forKey: defaultPlaylistKey.key) {
        let playlistName = self.appDel.joinPlaylistName
            print("Printing Playlist Name")
            print(playlistName) // Some String Value
            
        self.ref.child("playlists").child("\(playlistName)").observe(.value, with: { snapshot in
            
            var audioObject : [AudioObjectClass] = []
            for item in snapshot.children {
                
                //print(item as! FIRDataSnapshot)
                let newSong = AudioObjectClass(snapshot: item as! FIRDataSnapshot)
                audioObject.append(newSong)
                
            }
        
            audioObjects = audioObject
            
            self.queueTable.reloadData()
        })
        
        
        //This is to recieve what was updated on the database and appends it to audioObject array
        
        self.ref.child("playlists").child(playlistName).observe(.value, with: { snapshot in
            
            var audioObject : [AudioObjectClass] = []
            for item in snapshot.children {
                
                print(item as! FIRDataSnapshot)
                let newSong = AudioObjectClass(snapshot: item as! FIRDataSnapshot)
                audioObject.append(newSong)
                
            }
            
            audioObjects = audioObject
            
            self.queueTable.reloadData()
        })
//    }
        self.navigationItem.title = appDel.joinPlaylistName
}
        
    
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async(execute: { () -> Void in
            self.queueTable.reloadData()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.tintColor = UIColor.gray
    }
    // MARK: Button Functions
    
    @IBAction func reloadData(_ sender: AnyObject) {
        self.queueTable.reloadData()
    }
    
    @IBAction func disconnect(_ sender: AnyObject) {
        
        //self.defaults.setValue("StringKey", forKey: defaultPlaylistKey.key)
        self.appDel.joinPlaylistName = ""
        self.performSegue(withIdentifier: "NewPlaylist", sender: self)
        appDel.mainPlayer.stop()
    }
    
    // MARK: Table Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return audioObjects.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.queueTable.deselectRow(at: indexPath, animated: true)
        self.queueTable.reloadData()
        
    }
    
    func reloadTable(){
        self.queueTable.reloadData()
}
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        navigationBar.topItem?.title = "The Q"
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
