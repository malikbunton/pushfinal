//
//  PlaylistCollection.swift
//  Push
//
//  Created by Nashawn Chery on 1/14/18.
//  Copyright © 2018 Malik Bunton. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class PlaylistCollection {
    var playListTitle: String!
    var image: UIImage?
    
    func initWithProperties(PlayListTitle playListTitle: String,
                            Image image: UIImage){
        
        self.playListTitle = playListTitle
        self.image = image
        
    }
    
    convenience init(snapshot: FIRDataSnapshot){
        self.init()
        let snapshotValue = snapshot.value as! [String: AnyObject]
        self.playListTitle = snapshotValue["kDataHandlerKeyFor_AudioObjectPlaylistName"] as! String
        let artEncodedString: String =
            (snapshotValue[kDataHandlerKeyFor.AudioObjectArtwork] as? String)!
        let artData: Data = Data.init(base64Encoded: artEncodedString,
                                      options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
        self.image =  UIImage.init(data: artData as Data)!
    }

}
