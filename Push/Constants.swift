//
//  Constants.swift
//  Push
//
//  Created by Malik Bunton on 6/20/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//

import Foundation

enum songType { case link, ipod, ipodStream, streamServ, `undef`, appleMusic, soundCloud, audioMac,
spotify }

var audioObjects : [AudioObjectClass] = []
var playlistCollections : [PlaylistCollection] = []
var kUserDefaultsKeyJoinMode = "joinMode"
var kReloadHostNotificationName = "ReloadHost"
var kReloadJoinNotificationName = "ReloadUser"
var AppleMusicSearchCellID = "AppleMusicSearchCell"

//All the dictionary keys for transfering data (Capitalized).
struct kDataHandlerKeyFor{
    static let Data = "kDataHandlerKeyFor_Data"
    static let PeerID = "kDataHandlerKeyFor_PeerID"

    static let DataType = "kDataHandlerKeyFor_DataType"
    static let RequestType = "kDataHandlerKeyFor_RequestType"
    static let AudioObjectType = "kDataHandlerKeyFor_AudioObjectType"

    static let AudioObject = "kDataHandlerKeyFor_AudioObject"
    static let AudioObjectID = "kDataHandlerKeyFor_AudioObjectID"
    static let AudioObjectUUID = "kDataHandlerKeyFor_AudioObjectUUID"
    static let AudioObjectPlaylistName = "kDataHandlerKeyFor_AudioObjectPlaylistName"
    static let AudioObjectTitle = "kDataHandlerKeyFor_AudioObjectTitle"
    static let AudioObjectArtist = "kDataHandlerKeyFor_AudioObjectArtist"
    static let AudioObjectArtwork = "kDataHandlerKeyFor_AudioObjectArtwork"
    
    static let RemoveFirstObject = "kDataHandlerKeyFor_RemoveFirstObject"
    static let IndexOfMovedObject = "kDataHandlerKeyFor_IndexOfMovedObject"
}

struct kDataHandlerDataTypeOptions {
    static let request = "kDataHandlerDataTypeOptions_request"
    static let audioObject = "kDataHandlerDataTypeOptions_audioObject"
}

struct kDataHandlerRequestTypeOptions {
    static let rearrange = "kDataHandlerRequestTypeOptions_rearrange"
    static let delete = "kDataHandlerRequestTypeOptions_delete"
    static let songRequest  = "kDataHandlerRequestTypeOptions_songRequest"
}

struct kDataHandlerAudioObjectTypeOptions {
    static let link = "kDataHandlerAudioObjectTypeOptions_Link"
    static let ipodStream = "kDataHandlerAudioObjectTypeOptions_ipodStream"
    static let appleMusic = "kDataHandlerAudioObjectTypeOptions_appleMusic"
    static let spotify = "kDataHandlerAudioObjectTypeOptions_spotify"
    static let audioMack = "kDataHandlerAudioObjectTypeOptions_audioMack"
}

struct kDataHandlerRemoveFirstObjectOptions {
    static let aNo = 1
    static let aYes = 2
}

//This is the defaults to store the playlist name

struct defaultPlaylistKey {
    static var key = "StringKey"
}
