//
//  AudioStream.h
//  Push
//
//  Created by Nashawn Chery on 4/11/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//


//  AudioStream.h
//  AudioStreamer
//
//  Created by Tony DiPasquale on 10/4/13.
//  Copyright (c) 2013 Tony DiPasquale. The MIT License (MIT).
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, AudioStreamEvent) {
    AudioStreamEventHasData,
    AudioStreamEventWantsData,
    AudioStreamEventEnd,
    AudioStreamEventError
};

@class AudioStream;

@protocol AudioStreamDelegate <NSObject>

@required
- (void)audioStream:(AudioStream *)audioStream didRaiseEvent:(AudioStreamEvent)event;

@end

@interface AudioStream : NSObject

@property (assign, nonatomic) id<AudioStreamDelegate> delegate;

- (instancetype)initWithInputStream:(NSInputStream *)inputStream;
- (instancetype)initWithOutputStream:(NSOutputStream *)outputStream;

- (void)open;
- (void)close;
- (UInt32)readData:(uint8_t *)data maxLength:(UInt32)maxLength;
- (UInt32)writeData:(uint8_t *)data maxLength:(UInt32)maxLength;

@end
