//
//  SimpleCellClassTableViewCell.swift
//  Push
//
//  Created by Malik Bunton on 3/29/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//

import UIKit

class SimpleCellClassTableViewCell: UITableViewCell {
    
    @IBOutlet var artist: UILabel!
    @IBOutlet var picture: UIImageView!
    @IBOutlet var songTitle: UILabel!
    @IBOutlet var sender: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
