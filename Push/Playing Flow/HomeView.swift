//
//  HomeView.swift
//  Push
//
//  Created by Malik Bunton on 2/1/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//

import UIKit
import MultipeerConnectivity
import Firebase
import FirebaseDatabase

class HomeView: UIViewController {
    
    @IBOutlet weak var joinButton: UIStackView!
    @IBOutlet var hostButton: UIBarButtonItem!
    var image = UIImageView.newAutoLayout()
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var ref: FIRDatabaseReference!
    var user = FIRAuth.auth()?.currentUser
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TODO(Malik) Move this NSUserDefaults stuff
        UserDefaults.standard.set(false, forKey: kUserDefaultsKeyJoinMode)
        let bottomColor = UIColor(red: 255/255.0, green: 69/255.0, blue: 0/255.0, alpha: 1)
        let topColor = UIColor(red: 255/255.0, green: 165/255.0, blue: 0/255.0, alpha: 1)
        let gradientColors: [CGColor] = [topColor.cgColor, bottomColor.cgColor]
        let gradientLocation:[Float] = [0.0, 1.5]
        
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.locations = gradientLocation as [NSNumber]?
        
        gradientLayer.frame = self.view.bounds
       // self.view.layer.insertSublayer(gradientLayer, at: 0)
        image.image = #imageLiteral(resourceName: "launchpush")
        image.frame = CGRect(x: 0, y: 0, width: 5, height: 10)
//        image.autoSetDimensions(to: CGSize(width: 15, height: 300))
        self.navigationItem.titleView = image
        self.navigationItem.backBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "backBar"), style: .plain, target: nil, action: nil)
        UINavigationBar.appearance().tintColor = UIColor.black

    }
    
//    override var preferredStatusBarStyler: UIStatusBarStyle{
//        return .lightContent
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        displayWalkThrough()
        
    }
    
    func displayWalkThrough(){
        let displayedWalkThrough = defaults.bool(forKey: "DisplayedWalkThrough")
        if (!displayedWalkThrough){
            if let pageViewController = storyboard?.instantiateViewController(withIdentifier: "PageViewController"){
                self.present(pageViewController, animated: true, completion: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "officialJoin" {
            let joinModeOn =
                UserDefaults.standard.bool(forKey: kUserDefaultsKeyJoinMode)
            if(!joinModeOn) {
                UserDefaults.standard.set(true, forKey: kUserDefaultsKeyJoinMode)
            }
        }
    }

// MARK: Browser Delegate Methods
    
    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func hostButtonPressed(_ sender: AnyObject) {
        var tField: UITextField!
        self.ref = FIRDatabase.database().reference()
        
        func configurationTextField(_ textField: UITextField!) {
            textField.placeholder = "Enter a Playlist Name"
            tField = textField
        }
        
        let alert = UIAlertController(title: "Enter Playlist Name to Host", message: "Name Your Playlist", preferredStyle: .alert)
        alert.addTextField(configurationHandler: configurationTextField)
        
        alert.addAction(UIAlertAction(title: "Create", style: .default, handler: { (action) -> Void in
            print((tField?.text)!)
            if tField.text != "" {
                
                self.ref.child("playlists").child((tField?.text)!).observe(.value, with: { snapshot in
                    if snapshot.exists(){
                        
                        let duplicateAlert = UIAlertController(title: "Playlist Name Already Exists, Choose another Name", message: "", preferredStyle: .alert)
                        duplicateAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                            return
                        }))
                        self.present(duplicateAlert, animated: true, completion: nil)
                    
                    }else {
                        self.appDelegate.hostPlaylistName = tField.text!

                        let playlistName = self.appDelegate.hostPlaylistName
                        alert.dismiss(animated: true, completion: nil)
                        self.ref.child("playlists").child(playlistName)
                        let tabBar = TabBarViewController()
                        self.present(tabBar, animated: true, completion: nil)
                    }
                })
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {(action) -> Void in
        }))
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func reHostButtonPressed(_ sender: Any) {
        var tField: UITextField!
        self.ref = FIRDatabase.database().reference()
        
        func configurationTextField(_ textField: UITextField!) {
            textField.placeholder = "Enter a Playlist Name"
            tField = textField
        }
        
        let alert = UIAlertController(title: "Enter Name of Playlist", message: "Enter name of previously hosted playlist to Re-Host", preferredStyle: .alert)
        alert.addTextField(configurationHandler: configurationTextField)
        
        alert.addAction(UIAlertAction(title: "Re-Host", style: .default, handler: { (action) -> Void in
            print((tField?.text)!)
            if tField.text != "" {
                self.ref.child("playlists").child((tField?.text)!).observeSingleEvent(of: .value, with: {
                    snapshot in
                    
                    if snapshot.exists(){
                        self.appDelegate.hostPlaylistName = tField.text!
                        self.performSegue(withIdentifier: "toHostSession", sender: self)
                    } else{
                        let unfoundPlaylistAlert = UIAlertController(title: "Cant locate playlist, Try Again", message: "", preferredStyle: .alert)
                        unfoundPlaylistAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                            return
                        }))
                        self.present(unfoundPlaylistAlert, animated: true, completion: nil)
                    }
                })
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {(action) -> Void in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func joinButtonPressed(_ sender: UIButton) {
        var tField: UITextField!
        self.ref = FIRDatabase.database().reference()
        //Creating an alert for user to type name of the playlist to Rehost
        
        func configurationTextField(_ textField: UITextField!) {
            textField.placeholder = "Enter Playlist Name"
            tField = textField
        }
        
        let alert = UIAlertController(title: "Playlist To Join", message: "Enter Name of Playlist to Join", preferredStyle: .alert)
        alert.addTextField(configurationHandler: configurationTextField)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
             print((tField?.text)!)
            self.ref.child("playlists").child((tField?.text!)!).observeSingleEvent(of: .value, with: { snapshot in
    
                if snapshot.exists(){
                    self.appDelegate.joinPlaylistName = (tField?.text!)!
                    self.performSegue(withIdentifier: "officialJoin", sender: self)
                } else{
                    let unfoundPlaylistAlert = UIAlertController(title: "Cant locate playlist, Try Again", message: "", preferredStyle: .alert)
                    unfoundPlaylistAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                        return
                    }))
                    self.present(unfoundPlaylistAlert, animated: true, completion: nil)
                }
            })
            
            alert.dismiss(animated: true, completion: nil)
    }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {(action) -> Void in
        }))
        self.present(alert, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = 3
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}
