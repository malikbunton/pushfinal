//
//  QueueViewController.swift
//  Push
//
//  Created by Malik Bunton on 2/1/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//

import UIKit
import MediaPlayer
import MultipeerConnectivity
import Firebase


class QueueViewController: UIViewController, UITableViewDataSource,  UITableViewDelegate {

    @IBOutlet var goAddLinkButton: UIBarButtonItem!
    @IBOutlet var queueTable: UITableView!
    @IBOutlet weak var disconnectButton: UIBarButtonItem!
    
    var appDel: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var cellID = "queueCell"
    var indexOfCurrentSong = 0
    var ref: FIRDatabaseReference!
    var user = FIRAuth.auth()?.currentUser
    let defaults = UserDefaults.standard
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        queueTable.dataSource = self
        queueTable.delegate = self
        queueTable.tableFooterView = UIView()
        
        let nib = UINib(nibName: "CellDesignIpod", bundle: nil)
        queueTable.register(nib, forCellReuseIdentifier: "SimpleCell")

        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kReloadHostNotificationName), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadTable), name: NSNotification.Name(rawValue: kReloadHostNotificationName), object: nil)
        
  //This is to recieve what was updated on the database and appends it to audioObject array
        
        activityIndicator.center = self.queueTable.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
        queueTable.separatorStyle = UITableViewCellSeparatorStyle.none
        activityIndicator.startAnimating()
        queueTable.backgroundView = activityIndicator
        if self.appDel.hostPlaylistName != ""{
            DispatchQueue.global(qos: .background).async {

                self.ref = FIRDatabase.database().reference()
                
                    self.ref.child("playlists").child(self.appDel.hostPlaylistName).observe(.value, with: { snapshot in
                    
                    var audioObject : [AudioObjectClass] = []
                    for item in snapshot.children {
                        let newSong = AudioObjectClass(snapshot: item as! FIRDataSnapshot)
                        audioObject.append(newSong)
                        
                    }
                    
                    audioObjects = audioObject
                    
                })
            }
               DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.activityIndicator.stopAnimating()
                    self.queueTable.reloadData()
            }
        }
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    @IBAction func goToAddMedia(_ sender: AnyObject) {
        if appDel.hostPlaylistName == "" && appDel.joinPlaylistName == ""{
            self.performSegue(withIdentifier: "NewPlaylist", sender: self)
        }else{
            self.performSegue(withIdentifier: "toAddLink", sender: self)
        }
    }

    
    @IBAction func disconnect(_ sender: AnyObject) {

        self.appDel.hostPlaylistName = ""
        NowPlayingView().clearNowPlaying()
        self.performSegue(withIdentifier: "NewPlaylist", sender: self)
        appDel.mainPlayer.stop()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if self.appDel.hostPlaylistName != ""{
            DispatchQueue.main.async(execute: { () -> Void in
                self.ref = FIRDatabase.database().reference()
                
                self.ref.keepSynced(true)
                self.ref.child("playlists").child(self.appDel.hostPlaylistName).observe(.value, with: { snapshot in
                    
                    var audioObject : [AudioObjectClass] = []
                    for item in snapshot.children {
                        
                        let newSong = AudioObjectClass(snapshot: item as! FIRDataSnapshot)
                        audioObject.append(newSong)

                    }
                    
                    audioObjects = audioObject

                    self.queueTable.reloadData()
                    
                })
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.tintColor = UIColor.black
        
    }
    
    //TABLE METHODS
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SimpleCellClassTableViewCell = queueTable.dequeueReusableCell(withIdentifier: "SimpleCell", for: indexPath as IndexPath) as! SimpleCellClassTableViewCell
        
        //This is where every item in the audoObjects array populates every index path or cell
        
        let audioObject = audioObjects[indexPath.row]
        
        if let newTitle = audioObject.title {
            cell.songTitle.text = newTitle as String
        } else{ cell.songTitle.text = nil }
        if let newArtist = audioObject.artist {
            cell.artist.text = newArtist as String
        } else{ cell.artist.text = nil }
        if let newImage = audioObject.image {
            cell.picture.image = newImage
        } else{ cell.picture.image = nil }
        if let newSender = audioObject.sender {
            cell.sender.text = newSender as String
        }else{ cell.sender.text = nil }
        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return audioObjects.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//TODO(Malik): This is way too long of a function, delagate actions to new methods
        //if its not already up now
        if audioObjects.isEmpty {
            return
        }
        //This is all to remove the song from the queue after played, which I dont think we should do.
        
        
//        if (indexPath as NSIndexPath).row != 0 {
//            //if the player is playing remove the first object else push the second object down
//             var dataDict: [String: AnyObject] =
//                [kDataHandlerKeyFor.DataType: kDataHandlerDataTypeOptions.request as AnyObject]
//            dataDict[kDataHandlerKeyFor.RemoveFirstObject] =
//                kDataHandlerRemoveFirstObjectOptions.aNo as AnyObject?
//
//            let temp = audioObjects[(indexPath as NSIndexPath).row]
//            audioObjects.remove(at: (indexPath as NSIndexPath).row)
//            
//            //Trying to delete the selected song from the database
//            self.defaults.synchronize()
//            self.ref = FIRDatabase.database().reference()
//            
//            print("checking playlist name")
//            print(defaultPlaylistKey.key)
//            
//            if defaultPlaylistKey.key == "StringKey" {
//                let dataBase = temp.dataBaseID!
//                print(dataBase)
//                let playlistName = self.appDel.hostPlaylistName
//                let query = ref.child("playlists").child(playlistName).queryOrdered(byChild: dataBase)
//                print(query)
//                
//                //var dataBase = dataDict[kDataHandlerKeyFor.AudioObjectDataBaseID]!
//                
//                self.ref.child("playlists").child(playlistName).child(dataBase).removeValue()
//    
//            } else{
//                let playlistName = defaultPlaylistKey.key
//                print("/////////////")
//                print(playlistName)
//                self.ref.child("playlists").child(playlistName).childByAutoId().removeValue()
//            }
//            //TODO(Malik): Implement an universal player that include mpmusicplayer and the streamer
//            //player so this if statment can call "universalPLayer.playbackState()" not just for mpplayer.
//            if appDel.mainPlayer.playbackState == MPMusicPlaybackState.playing {
//                audioObjects.removeFirst()
//                dataDict[kDataHandlerKeyFor.RemoveFirstObject] =
//                    kDataHandlerRemoveFirstObjectOptions.aYes as AnyObject?
//            }
//            audioObjects.insert(temp, at: 0)
//            dataDict[kDataHandlerKeyFor.RequestType] = kDataHandlerRequestTypeOptions.rearrange as AnyObject?
//            dataDict[kDataHandlerKeyFor.IndexOfMovedObject] = (indexPath as NSIndexPath).row as Int as AnyObject?
//
//            DataSenderClass().handleOutGoingRequests(dataDict as NSDictionary, peer: nil)
//            handleMusicContent(0);
//        }
//        else {
//            if(self.appDel.mainPlayer.playbackState != MPMusicPlaybackState.playing) {
//                handleMusicContent((indexPath as NSIndexPath).row)
//            }
//        }
//        print("Index Path")
//        print((indexPath as NSIndexPath).row)
//        appDel.counter = (indexPath as NSIndexPath).row
//        handleMusicContent((indexPath as NSIndexPath).row)
//        self.queueTable.deselectRow(at: indexPath, animated: true)
//        self.queueTable.reloadData()
    }

    func tableView(_ tableView: UITableView,commit editingStyle: UITableViewCellEditingStyle,
                   forRowAt indexPath: IndexPath) {
        let indexDeleted = indexPath.row
        let audioObject = audioObjects[indexDeleted]
    
        let playlistname = self.appDel.hostPlaylistName
        self.ref.child("playlists").child(playlistname).child(audioObject.uuid!).removeValue(completionBlock:{ (error, completion) in
                if error != nil {
                    print(error as Any)
                } else {
                    print("Child Removed Correctly")
//                    if (self.appDel.tracksArray.count != 0) {
//                        for i in 0...(self.appDel.tracksArray.count-1){
//                        if(self.appDel.tracksArray[i] == audioObject.id){
//                            self.appDel.tracksArray.remove(at: i)
//                            self.appDel.counter -= 1
//                            self.appDel.numSongsDeleted += 1
//                            break
//                            }
//                        }
//                    }
                }
        })
        if (indexDeleted <= appDel.nowPlayinIndex){
            if(appDel.nowPlayinIndex < 0){
            appDel.nowPlayinIndex -= 1
            }
        }
         audioObjects.remove(at: indexDeleted)
    }
    
    
    func reloadTable(){
        self.queueTable.reloadData()
    }

// MARK: Content Handling

    func handleMusicContent(_ index: Int) {
        if (index < audioObjects.count ){
            if(index == 0){
                self.appDel.currentSong = audioObjects[index]
                //NowPlayingView().setQueueWithCurrentSong(true)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
     
     //If its an iPod song then play it
     if appDel.tableQ[0].mediaType == songType.Ipod{
     print("ipod")
     appDel.currentSong = MPMediaItemCollection(items: [appDel.tableQ[0].obj as! MPMediaItem])
     appDel.mainPlayer.setQueueWithItemCollection(appDel.currentSong)
     appDel.mainPlayer.play()
     }
     
     if appDel.tableQ[0].mediaType == songType.IpodStream{
     print("stream")
     appDel.mainPlayer.stop()
     
     if let passURl = appDel.tableQ[indexPath.row].obj.objectForKey("Object") as? String{
     appDel.passableURL = passURl
     self.tabBarController?.selectedIndex = 0
     }
     }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
