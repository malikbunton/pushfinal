//
//  AddAppleMuiscCellTableViewCell.swift
//  Push
//
//  Created by Malik Bunton on 11/3/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//

import UIKit

class AppleMusicSearchTableViewCell: UITableViewCell {
    var isSeltected: Bool! = false
    @IBOutlet var picture: UIImageView!
    @IBOutlet var songTitle: UILabel!
    @IBOutlet var artist: UILabel!
    @IBOutlet var mediaType: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addButton.layer.borderWidth = 1
        addButton.layer.borderColor = UIColor.orange.cgColor
        isSeltected = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func changeSelectedStateVal(_ sender: AnyObject) {
        if isSeltected == true {
            isSeltected = false
            addButton.backgroundColor = UIColor.clear
            print("\(songTitle.text!): \(isSeltected)")
        }
        else {
            isSeltected = true
            addButton.backgroundColor = UIColor.orange
        }
    }
    
    private func changeSelectedState() {
        if isSeltected == true {
            isSeltected = false
            
            addButton.backgroundColor = UIColor.clear
        }
        else {
            isSeltected = true
            addButton.backgroundColor = UIColor.orange
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
}
