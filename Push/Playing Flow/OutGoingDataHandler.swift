//
//  OutGoingDataHandler.swift
//  Push
//
//  Created by Malik Bunton on 6/20/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//

import Foundation
import UIKit
import MultipeerConnectivity
import MediaPlayer
import Firebase
import FirebaseDatabase

class DataSenderClass {
    static let sharedInstance = DataSenderClass()
    var ref: FIRDatabaseReference!
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var user = FIRAuth.auth()?.currentUser
    let defaults = UserDefaults.standard

//MARK: Audio Objects
    
    internal func sendAudioObject(_ audioObject: AudioObjectClass) {
        print("this is not sending a real object right now")
    }
    
    internal func sendSelectedAppleMusicSong(_ selectedSong: AudioObjectClass) {
        var dataDict:[String: AnyObject] =
            [kDataHandlerKeyFor.DataType : kDataHandlerDataTypeOptions.audioObject as AnyObject]
        dataDict[kDataHandlerKeyFor.AudioObjectTitle] = selectedSong.title as? String as AnyObject?
        dataDict[kDataHandlerKeyFor.AudioObjectType] =
            kDataHandlerAudioObjectTypeOptions.appleMusic as AnyObject?
        dataDict[kDataHandlerKeyFor.AudioObjectID] = selectedSong.id as AnyObject
        dataDict[kDataHandlerKeyFor.AudioObjectArtist] = selectedSong.artist as? String as AnyObject?
        if let artWork = selectedSong.image{
        let imageData: Data = UIImagePNGRepresentation(artWork)! as Data
            let artData =
                imageData.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
            dataDict[kDataHandlerKeyFor.AudioObjectArtwork] = artData as AnyObject?
        }
        
        //Adds song to database
        self.ref = FIRDatabase.database().reference()
        
        print("checking playlist name")
        if self.appDelegate.hostPlaylistName != "" {
            let playlistName = self.appDelegate.hostPlaylistName
            if let user = FIRAuth.auth()?.currentUser {
                self.ref.child("users").child(user.uid).child("playlists").child(playlistName).observeSingleEvent(of: .value, with: { snapshot in
                    if !snapshot.exists(){
                        var playlistDict:[String: AnyObject] =
                            [kDataHandlerKeyFor.DataType : kDataHandlerDataTypeOptions.audioObject as AnyObject]

                        playlistDict[kDataHandlerKeyFor.AudioObjectPlaylistName] = playlistName as AnyObject
                        
                        if let artWork = selectedSong.image{
                            let imageData: Data = UIImagePNGRepresentation(artWork)! as Data
                            let artData =
                                imageData.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
                            playlistDict[kDataHandlerKeyFor.AudioObjectArtwork] = artData as AnyObject?
                        }
                       self.ref.child("users").child(user.uid).child("playlists").child(playlistName).setValue(playlistDict)
                    }
                })
            }

            let songRef = self.ref.child("playlists").child(playlistName).childByAutoId()
            selectedSong.uuid = songRef.key
            dataDict[kDataHandlerKeyFor.AudioObjectUUID] = songRef.key as AnyObject
            songRef.setValue(dataDict)
            
        } else{
            let playlistName = self.appDelegate.joinPlaylistName

            let songRef = self.ref.child("playlists").child(playlistName).childByAutoId()
            selectedSong.uuid = songRef.key
            dataDict[kDataHandlerKeyFor.AudioObjectUUID] = songRef.key as AnyObject
            songRef.setValue(dataDict)

        }
    }
    
//MARK: Queue Commands
    
}
