//
//  AppleMusicSearchViewController.swift
//  Push
//
//  Created by Nashawn Chery on 8/1/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//

import UIKit
import MediaPlayer

class AppleMusicSearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, MPMediaPickerControllerDelegate {
    
    @IBOutlet var doneButton: UIBarButtonItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var returnTable: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    @IBOutlet weak var addFromLibrary: UIButton!
    var songArray = Array<Dictionary<String, String>>()
    var returned = true
    var maxObjectsReturned = 8
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var appDel: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchBar.delegate = self
        returnTable.tableFooterView = UIView()
        returnTable.dataSource = self
        returnTable.delegate = self
        let nib = UINib(nibName: "AddMusicCell", bundle: nil)
        returnTable.register(nib, forCellReuseIdentifier: AppleMusicSearchCellID)
        
    }
    
    public func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection){
        if #available(iOS 10.3, *) {
            let newAudioObject = AudioObjectClass()
            for i in mediaItemCollection.items{
                if let setTitle: NSString = i.title! as String as NSString? {
                    newAudioObject.title = setTitle
                }
                if let setArtist: NSString = i.artist! as String as NSString? {
                    newAudioObject.artist = setArtist
                }
            
                newAudioObject.id = i.playbackStoreID
    
                if let image = i.artwork {
                    let size = CGSize(width: 500, height: 500)
                    newAudioObject.image = image.image(at: size)
            
                }
                DataSenderClass().sendSelectedAppleMusicSong(newAudioObject)
                self.dismiss(animated: true, completion: nil)
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    public func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addFromLibrary(_ sender: Any) {
         if #available(iOS 10.3, *) {
            MPMediaLibrary.requestAuthorization { (status) in
                if status == .authorized {
            let library = MPMediaPickerController.self(mediaTypes: MPMediaType.music)
            library.delegate = self
            library.allowsPickingMultipleItems = true
            self.present(library, animated: true, completion: nil)
                } else{
                    // TODO: Place Pop up to sign up for apple Music
                }
            }
        }
    }
    
    internal func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        songArray.removeAll()
        
        activityIndicator.center = self.returnTable.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
        returnTable.separatorStyle = UITableViewCellSeparatorStyle.none
        activityIndicator.startAnimating()
        returnTable.backgroundView = activityIndicator
        let newString = searchBar.text!.replacingOccurrences(of: " ", with: "+")
        DispatchQueue.global(qos: .background).async {
        let request =
            URLRequest(url: URL(string: "http://itunes.apple.com/search?term=" +
                newString + "&limit=\(self.maxObjectsReturned)")!)
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {data, response, error ->
            Void in print("Response: \(String(describing: response))")
            
            let json = try! JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as![String:AnyObject]
            
        
            print("Printing JSon")
            print(json)
            print("printing results...")
            
            let results: [[String:AnyObject]] = json["results"] as! [[String : AnyObject]];
        
            if(results.count == 0){
                self.songArray.removeAll()
                self.returned = false
                self.reloadTable()
                 }else{
                self.returned = true
                for idx in 0...(results.count-1) {
                    var songDict = Dictionary<String, String>()
                    if results[idx]["kind"] as? String != "song"{
                        continue
                    }
                    songDict["trackName"] = results[idx]["trackName"] as? String
                    songDict["artistName"] = results[idx]["artistName"] as? String
                    songDict["artwork"] = results[idx]["artworkUrl100"] as? String
                    songDict["trackId"] = String(results[idx]["trackId"] as! Int)
                    songDict["mediaKind"] = results[idx]["kind"] as? String
                    self.songArray.append(songDict)
                }
                print(self.songArray)
                self.reloadTable()
            }
        })
       // task.resume()
//        print(self.songArray)
//        searchBar.resignFirstResponder()

    
         DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            task.resume()
            searchBar.resignFirstResponder()
            self.activityIndicator.stopAnimating()
            self.reloadTable()
        }
        
    }

        //self.reloadTable()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            if returned == false{
            songArray.removeAll()
            let emptyLabel = UILabel(frame: CGRect(x:0, y:0, width:self.view.bounds.size.width, height:self.view.bounds.size.height))
            emptyLabel.text = "No Data"
            emptyLabel.textColor = UIColor.white
            emptyLabel.textAlignment = NSTextAlignment.center
            tableView.backgroundView = emptyLabel
            tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            return 0
            }
        }
        return songArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: AppleMusicSearchTableViewCell =
            returnTable.dequeueReusableCell(withIdentifier: AppleMusicSearchCellID, for: indexPath) as! AppleMusicSearchTableViewCell
        
        if (self.returned){
        let song = songArray[(indexPath as NSIndexPath).item]
            
            //cell.addButton.addTarget(self, action: #selector(AppleMusicSearchTableViewCell.changeSelectedStateVal()), for: .TouchUpInside)

            cell.addButton.isUserInteractionEnabled = true
        cell.songTitle.text = song["trackName"]! as String
        cell.artist.text = song["artistName"]! as String
        cell.mediaType.text = song["mediaKind"]!.capitalized
        cell.isSeltected = false
        cell.addButton.backgroundColor = UIColor.clear
        //print("Song artwork: " + song["artwork"]!)
        
        if let url: String = song["artwork"] {
            let urlA = url.replacingOccurrences(of: "100x100", with: "500x500")
            print(urlA)
            if let imgURL = URL(string: urlA) {
                if let data = try? Data(contentsOf: imgURL) {
                    cell.picture.image = UIImage(data: data)
                }
            }
        }
        }
   
        return cell
    }
    
    func reloadTable(){
        DispatchQueue.main.async(execute: { () -> Void in
            self.returnTable.reloadData()
        })
    }
    
    @IBAction func cancelPressed(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func donePressed(_ sender: AnyObject) {
        addSelctedSongs(selectedCellIndex: indexOfSelectedCells(), completion: self.dismiss(animated: true, completion: nil))
    }
    
    private func tablePaths() -> [NSIndexPath] {
        if self.returnTable.numberOfRows(inSection: 0) < 1 {
            return []
        }
        var paths = [NSIndexPath]()
        
        for sectionNum in 0...(self.returnTable.numberOfSections - 1) {
            //Cycles through all the cells in the table
            for rowNum in 0...(self.returnTable.numberOfRows(inSection: sectionNum) - 1) {
                
                let path = NSIndexPath(row: rowNum, section: sectionNum)
                paths.append(path)
            }
        }
        return paths
    }
    
    private func indexOfSelectedCells() -> [Int] {
        var cellIndex: [Int] = []
        let numOfCells = songArray.count
        var indexes = tablePaths()
        if (indexes.count == 0){
            return []
        }
        for row in 0...(indexes.count-1){
            print("index path :")
            print(indexes[row] as IndexPath)
            print(indexes.count)
            if let song =
                (returnTable.cellForRow(at: (indexes[row] as IndexPath)) as? AppleMusicSearchTableViewCell){
                let songCell : AppleMusicSearchTableViewCell = (song as AppleMusicSearchTableViewCell)
                if (songCell.isSeltected!) {
                    cellIndex.append(row)
                    print("Row Number:")
                    print(row)
                    print (" ")
                }
            }
        }
        print(cellIndex)
        return cellIndex
    }
    
    private func addSelctedSongs(selectedCellIndex: [Int], completion: ()) {
        // When you select an apple music song
        if (selectedCellIndex == []){
            return
        }
       
        for rowNumber in 0...(selectedCellIndex.count - 1) {
            
             // Creates an instance of AudioObject class and fills the results from the search into the instance
            let newAudioObject = AudioObjectClass()
            
            newAudioObject.mediaType = songType.appleMusic
            
            let song = songArray[selectedCellIndex[rowNumber]]
            
            if let setTitle: NSString = song["trackName"]! as String as NSString? {
                newAudioObject.title = setTitle
            }
            if let setArtist: NSString = song["artistName"]! as String as NSString? {
                newAudioObject.artist = setArtist
            }
//            let productId: NSString = song["trackId"]! as String as NSString
            
//            if let uID:Int64 = productId.longLongValue {
//                let numId = NSNumber(value: uID as Int64)
//                newAudioObject.id = numId
                newAudioObject.id = song["trackId"]!
                print("New Object")
                print(newAudioObject.id!)
            
//                appDel.tracksArray.append(song["trackId"]!)
                //
//            }
            if let url: String = song["artwork"] {
                let urlA = url.replacingOccurrences(of: "100x100", with: "500x500")
                if let imgURL = URL(string: urlA) {
                    if let data = try? Data(contentsOf: imgURL) {
                        print(data)
                        newAudioObject.image = UIImage(data: data)
                        
                    }
                }
            }
            
    
            //Sender will always be 'Me' to the user. It gets read and changed when info is sent out.
            newAudioObject.sender = "Me"
            
            // Appends the new instance of the class to audioObjects array, which populates the Queue Table, but I commented it out and appended it to the array after it detects the database has been updated
            
            //audioObjects.append(newAudioObject)
            
            DataSenderClass().sendSelectedAppleMusicSong(newAudioObject)
        }
//        NowPlayingView().setQueueWithCurrentSong(false)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
}
