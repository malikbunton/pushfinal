//
//  NowPlayingView.swift
//  Push
//
//  Created by Malik Bunton on 2/1/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//

import UIKit
import MediaPlayer
import MultipeerConnectivity
import Firebase
import MediaPlayer

class NowPlayingView: UIViewController{
    var mpVolumeView: MPVolumeView!
    
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var playbackLabel: UILabel!
    @IBOutlet var volumeBarView: UIView!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var playView: UIView!
    @IBOutlet weak var songName: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var albumArtWork: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    
    var appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var updater : CADisplayLink! = nil
    var timer: Timer? = nil
    var updateTimer: Timer? = nil
    var currentPlayback: NSString?
    var time: Int? = nil
    var nowPlayingTime: String? = nil
    var currentDuration: Double? = nil
    var ref: FIRDatabaseReference!
    let commandCenter = MPRemoteCommandCenter.shared()
    
    
// MARK: UI Initialization
    

   override func viewDidLoad() {
        super.viewDidLoad()
        playbackLabel.text = "0:00"
        self.progressBar.setProgress(0, animated: true)
        mpVolumeView = MPVolumeView(frame: self.volumeBarView.bounds)
        mpVolumeView.tintColor = UIColor.white
        volumeBarView.addSubview(mpVolumeView)
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.tintColor = UIColor.gray
        appDelegate.viewPresented = true
        if audioObjects.count != 0 {
            //Come Back to Optimize !! Issue is with when nothing has been played yet and loading Now Playing Info
            if (durationLabel.text == "0:00"){
                self.appDelegate.currentSong = audioObjects[0]
                update()
            }
            if (self.appDelegate.mainPlayer.playbackState == MPMusicPlaybackState.playing){
                self.startTimer()
            }
            if (self.appDelegate.mainPlayer.playbackState == MPMusicPlaybackState.playing || self.appDelegate.mainPlayer.playbackState == MPMusicPlaybackState.paused){
//                updateNowPlayingInfo(audioObjects[(self.appDelegate.counter - (self.appDelegate.tracksArray.count - 1)) + self.appDelegate.mainPlayer.indexOfNowPlayingItem])
                updateNowPlayingInfo(appDelegate.currentSong!)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool){
        appDelegate.viewPresented = false
    
    }
    
    
    func configureCommandCenter() {
        self.commandCenter.nextTrackCommand.isEnabled = true
        self.commandCenter.nextTrackCommand.addTarget(self, action: #selector(NowPlayingView.nextSong(_:)))
    }
    
    
    @IBAction func playMusic(_ sender: AnyObject) {
//        if (appDelegate.tracksArray.count == 0){
//            if(audioObjects.count != 0){
//                for i in audioObjects{
//                    appDelegate.tracksArray.append(i.id!)
//                }
//                setQueueWithCurrentSong(true)
//                self.startTimer()
//                appDelegate.mainPlayer.beginGeneratingPlaybackNotifications()
//                NotificationCenter.default.addObserver(self, selector: #selector(NowPlayingView.updatePlaylist),
//                                                       name:NSNotification.Name.MPMusicPlayerControllerNowPlayingItemDidChange,
//                                                       object: self.appDelegate.mainPlayer)
//            }else{
//                return
//            }
//        }
        if(self.appDelegate.mainPlayer.playbackState == MPMusicPlaybackState.playing){
            self.appDelegate.mainPlayer.pause()
            self.stopTimer()
        } else if (self.appDelegate.mainPlayer.playbackState == MPMusicPlaybackState.paused) {
                self.appDelegate.mainPlayer.play()
                self.startTimer()
        }
        else{
            if(audioObjects.count != 0){
//                self.configureCommandCenter()
                appDelegate.mainPlayer.beginGeneratingPlaybackNotifications()
                NotificationCenter.default.addObserver(self, selector: #selector(NowPlayingView.updatePlaylist),
                                                       name:NSNotification.Name.MPMusicPlayerControllerNowPlayingItemDidChange,
                                                       object: self.appDelegate.mainPlayer)
                self.appDelegate.nowPlayinIndex = 0
                self.appDelegate.currentSong = audioObjects[self.appDelegate.nowPlayinIndex]
                setQueueWithCurrentSong(true)
                self.startTimer()
            }
        }
    }
    
    func playSelectedSong(){
        if (self.appDelegate.mainPlayer.currentPlaybackRate == 1.0){
            return
        }
        self.appDelegate.counter += appDelegate.tracksArray.count - 1
        self.appDelegate.currentQueueLen = appDelegate.tracksArray.count - 1
        if (timer == nil) {
            self.appDelegate.mainPlayer.play()
            self.startTimer()
        }else{
            self.stopTimer()
            self.appDelegate.mainPlayer.play()
            self.startTimer()
        }
    }

    @IBAction func nextSong(_ sender: AnyObject) {
        self.appDelegate.nextButtonClicked = true
        self.updatePlaylist()
//        appDelegate.mainPlayer.skipToNextItem()
//        appDelegate.currentSong = audioObjects[appDelegate.mainPlayer.indexOfNowPlayingItem]
    }

    
    func setQueueWithCurrentSong(_ playSong: Bool){
        appDelegate.currentSong = audioObjects[self.appDelegate.nowPlayinIndex]
        if (appDelegate.viewPresented!){
            updateNowPlayingInfo(appDelegate.currentSong!)
        }
    appDelegate.mainPlayer.setQueueWithStoreIDs([audioObjects[self.appDelegate.nowPlayinIndex].id!])
        self.appDelegate.mainPlayer.play()
//        appDelegate.mainPlayer.setQueueWithStoreIDs(appDelegate.tracksArray)
        if (playSong){
            self.playSelectedSong()
        }else{
            return
        }
    }
    
    func startTimer(){
        if timer == nil {
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(NowPlayingView.update), userInfo: nil,repeats: true)
                self.timer?.fire()
        }
    }
    
    func stopTimer(){
        timer = nil
    }
    
    func update(){
        if (self.appDelegate.mainPlayer.nowPlayingItem == nil){
            return
        }
        if (!appDelegate.viewPresented!){
            return
        }
        if (self.appDelegate.mainPlayer.currentPlaybackTime.magnitude.rounded().truncatingRemainder(dividingBy: 1) != 0){
            return
        }
        let time = abs(Int(self.appDelegate.mainPlayer
            .currentPlaybackTime.magnitude.rounded()))
        if let duration = self.appDelegate.mainPlayer.nowPlayingItem?.playbackDuration.magnitude.rounded(){
    
            if (duration != currentDuration){
                changeDurationLabel()
            }
            if (progressBar != nil){
                DispatchQueue.main.async {
                    self.progressBar.progress = Float(time)/Float(duration)
                    self.progressBar.setProgress(self.progressBar.progress, animated: true)
                }
            }
        }
        let seconds = abs(Int(self.appDelegate.mainPlayer
            .currentPlaybackTime.magnitude.rounded().truncatingRemainder(dividingBy: 60)))
        
        let minutes = Int(floor(abs(self.appDelegate.mainPlayer
            .currentPlaybackTime.magnitude.rounded() / 60)))
        
        if( seconds < 10 ){
            nowPlayingTime = "\(minutes):0\(seconds)"
        }else{
            nowPlayingTime = "\(minutes):\(seconds)"
        }
        if let dd = nowPlayingTime{
            if (playbackLabel != nil){
                DispatchQueue.main.async(execute: {
                    self.playbackLabel.text = dd
                })
            }
        }

    }
    
    func updateNowPlayingInfo(_ song: AudioObjectClass){
        changeAlbumArtwork(song)
        changeAlbumLabels(song)
    }
    
    func updatePlaylist(){
        while (self.appDelegate.nowPlayinIndex < audioObjects.count){
            if(self.appDelegate.mainPlayer.currentPlaybackTime.isNaN){
                self.appDelegate.nowPlayinIndex += 1
                setQueueWithCurrentSong(true)
            }
            if(self.appDelegate.nextButtonClicked){
                self.appDelegate.nextButtonClicked = false
                self.appDelegate.nowPlayinIndex += 1
                setQueueWithCurrentSong(true)
            }
        }
//        if (appDelegate.viewPresented!){
//            updateNowPlayingInfo(audioObjects[(self.appDelegate.counter - (self.appDelegate.tracksArray.count - 1)) + self.appDelegate.mainPlayer.indexOfNowPlayingItem])
//        }
//        if (self.appDelegate.mainPlayer.indexOfNowPlayingItem == appDelegate.tracksArray.count - 1){
//
//        self.appDelegate.mainPlayer.pause()
//
//        var FinalTracks = Array<String>()
//        DispatchQueue.main.async(execute: { () -> Void in
//            self.ref = FIRDatabase.database().reference()
//            self.ref.child("playlists").child(self.appDelegate.hostPlaylistName).observe(.value, with: { snapshot in
//                if (self.appDelegate.mainPlayer.currentPlaybackRate == 1.0){
//                    return
//                }
//                var newTracksArray = Array<String>()
//                for item in snapshot.children {
//                    let newSong = AudioObjectClass(snapshot: item as! FIRDataSnapshot)
//                    newTracksArray.append(newSong.id!)
//                }
//                for i in self.appDelegate.counter...newTracksArray.count - 1{
//                    FinalTracks.append(newTracksArray[i])
//                }
//                if(FinalTracks.count > 1){
//                    self.appDelegate.tracksArray = FinalTracks
//                    self.setQueueWithCurrentSong(true)
//                } else {
//                    self.appDelegate.mainPlayer.play()
//                    return
//                    }
//                })
//            })
//        }else{
//            if (self.appDelegate.currentQueueLen != self.appDelegate.tracksArray.count-1){
//                self.appDelegate.mainPlayer.pause()
//                self.appDelegate.counter -= (self.appDelegate.tracksArray.count - 1)
//                var NewArray = Array<String>()
//                for i in self.appDelegate.mainPlayer.indexOfNowPlayingItem...self.appDelegate.tracksArray.count - 1{
//                    NewArray.append(self.appDelegate.tracksArray[i])
//                }
//                self.appDelegate.tracksArray = NewArray
//                self.appDelegate.currentQueueLen = self.appDelegate.tracksArray.count - 1
//                self.appDelegate.counter += self.appDelegate.tracksArray.count - 1
//                self.appDelegate.numSongsDeleted = 0
//                self.setQueueWithCurrentSong(true)
//            } else {
//                return
//            }
//        }
    }

    func changeAlbumArtwork(_ song: AudioObjectClass) {
        if let image = song.image{
            let tempImage = MPMediaItemArtwork(image: image)
            albumArtWork.image = tempImage.image(at: self.albumArtWork.frame.size)
        }
    }

    func changeAlbumLabels(_ song: AudioObjectClass) {
        if let name = song.artist {
            artistName.text = name as String
        }
        if let songTitle = song.title {
            songName.text = songTitle as String
        }
        changeDurationLabel()
    }
    
    func changeDurationLabel(){
        if let duration = self.appDelegate.mainPlayer.nowPlayingItem?.playbackDuration.magnitude.rounded(){
            currentDuration = duration
            let durationInt: Int = Int(duration)
            let minutes = Int(duration / 60)
            let seconds = durationInt % 60
            if(durationLabel != nil){
                if( seconds < 10 ){
                    self.durationLabel.text = "\(minutes):0\(seconds)"
                }else{
                    self.durationLabel.text = "\(minutes):\(seconds)"
                }
            }
        }

    }
    
    func clearNowPlaying(){
        appDelegate.currentSong = nil
        if let duration = durationLabel{
            duration.text = "0:00"
        }
        if let playBack = playbackLabel{
            playBack.text = "0:00"
        }
        
        self.albumArtWork = nil
     
        if let song = songName {
            song.text = ""
        }
        if let artist = artistName{
            artist.text = ""
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


