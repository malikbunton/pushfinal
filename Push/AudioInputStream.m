//
//  AudioInputStream.m
//  Push
//
//  Created by Nashawn Chery on 4/11/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//


//
//  AudioInputStream.m
//  AudioStreamer
//
//  Created by Tony DiPasquale on 10/4/13.
//  Copyright (c) 2013 Tony DiPasquale. The MIT License (MIT).
//

#import <Foundation/Foundation.h>
#import "AudioInputStreamHeader.h"
#import "AudioFileStream.h"
#import "AudioStream.h"
#import "AudioQueue.h"
#import "AudioQueueBuffer.h"
#import "AudioQueueFiller.h"
#import "AudioStreamerConstants.h"

@interface AudioInputStreamHeader () <AudioStreamDelegate, TDAudioFileStreamDelegate, AudioQueueDelegate>

@property (strong, nonatomic) NSThread *audioStreamerThread;
@property (assign, atomic) BOOL isPlaying;

@property (strong, nonatomic) AudioStream *audioStream;
@property (strong, nonatomic) AudioFileStream *audioFileStream;
@property (strong, nonatomic) AudioQueue *audioQueue;

@end

@implementation AudioInputStream

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.audioFileStream = [[TDAudioFileStream alloc] init];
    if (!self.audioFileStream) return nil;
    
    self.audioFileStream.delegate = self;
    
    return self;
}

- (instancetype)initWithInputStream:(NSInputStream *)inputStream
{
    self = [self init];
    if (!self) return nil;
    
    self.audioStream = [[AudioStream alloc] initWithInputStream:inputStream];
    if (!self.audioStream) return nil;
    
    self.audioStream.delegate = self;
    
    return self;
}

- (void)start
{
    if (![[NSThread currentThread] isEqual:[NSThread mainThread]]) {
        return [self performSelectorOnMainThread:@selector(start) withObject:nil waitUntilDone:YES];
    }
    
    self.audioStreamerThread = [[NSThread alloc] initWithTarget:self selector:@selector(run) object:nil];
    [self.audioStreamerThread start];
}

- (void)run
{
    @autoreleasepool {
        [self.audioStream open];
        
        self.isPlaying = YES;
        
        while (self.isPlaying && [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]]) ;
    }
}

#pragma mark - Properties

- (UInt32)audioStreamReadMaxLength
{
    if (!_audioStreamReadMaxLength)
        _audioStreamReadMaxLength = kAudioStreamReadMaxLength;
    
    return _audioStreamReadMaxLength;
}

- (UInt32)audioQueueBufferSize
{
    if (!_audioQueueBufferSize)
        _audioQueueBufferSize = kAudioQueueBufferSize;
    
    return _audioQueueBufferSize;
}

- (UInt32)audioQueueBufferCount
{
    if (!_audioQueueBufferCount)
        _audioQueueBufferCount = kAudioQueueBufferCount;
    
    return _audioQueueBufferCount;
}

#pragma mark - AudioStreamDelegate

- (void)audioStream:(AudioStream *)audioStream didRaiseEvent:(AudioStreamEvent)event
{
    switch (event) {
        case AudioStreamEventHasData: {
            uint8_t bytes[self.audioStreamReadMaxLength];
            UInt32 length = [audioStream readData:bytes maxLength:self.audioStreamReadMaxLength];
            [self.audioFileStream parseData:bytes length:length];
            break;
        }
            
        case AudioStreamEventEnd:
            self.isPlaying = NO;
            [self.audioQueue finish];
            break;
            
        case AudioStreamEventError:
            [[NSNotificationCenter defaultCenter] postNotificationName:AudioStreamDidFinishPlayingNotification object:nil];
            break;
            
        default:
            break;
    }
}

#pragma mark - TDAudioFileStreamDelegate

- (void)audioFileStreamDidBecomeReady:(TDAudioFileStream *)audioFileStream
{
    UInt32 bufferSize = audioFileStream.packetBufferSize ? audioFileStream.packetBufferSize : self.audioQueueBufferSize;
    
    self.audioQueue = [[AudioQueue alloc] initWithBasicDescription:audioFileStream.basicDescription bufferCount:self.audioQueueBufferCount bufferSize:bufferSize magicCookieData:audioFileStream.magicCookieData magicCookieSize:audioFileStream.magicCookieLength];
    
    self.audioQueue.delegate = self;
}

- (void)audioFileStream:(TDAudioFileStream *)audioFileStream didReceiveError:(OSStatus)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:AudioStreamDidFinishPlayingNotification object:nil];
}

- (void)audioFileStream:(TDAudioFileStream *)audioFileStream didReceiveData:(const void *)data length:(UInt32)length
{
    [AudioQueueFiller fillAudioQueue:self.audioQueue withData:data length:length offset:0];
}

- (void)audioFileStream:(TDAudioFileStream *)audioFileStream didReceiveData:(const void *)data length:(UInt32)length packetDescription:(AudioStreamPacketDescription)packetDescription
{
    [AudioQueueFiller fillAudioQueue:self.audioQueue withData:data length:length packetDescription:packetDescription];
}

#pragma mark - AudioQueueDelegate

- (void)audioQueueDidFinishPlaying:(AudioQueue *)audioQueue
{
    [self performSelectorOnMainThread:@selector(notifyMainThread:) withObject:AudioStreamDidFinishPlayingNotification waitUntilDone:NO];
}

- (void)audioQueueDidStartPlaying:(AudioQueue *)audioQueue
{
    [self performSelectorOnMainThread:@selector(notifyMainThread:) withObject:AudioStreamDidStartPlayingNotification waitUntilDone:NO];
}

- (void)notifyMainThread:(NSString *)notificationName
{
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil];
}

#pragma mark - Public Methods

- (void)resume
{
    [self.audioQueue play];
}

- (void)pause
{
    [self.audioQueue pause];
}

- (void)stop
{
    [self performSelector:@selector(stopThread) onThread:self.audioStreamerThread withObject:nil waitUntilDone:YES];
}

- (void)stopThread
{
    self.isPlaying = NO;
    [self.audioQueue stop];
}

@end
