//
//  AddMediaViewController.swift
//  Push
//
//  Created by Nashawn Chery on 9/2/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//

import UIKit
import MediaPlayer

class AddMediaViewController: UIViewController,MPMediaPickerControllerDelegate {
    @IBOutlet weak var cancelButton: UIBarButtonItem!

    @IBOutlet weak var appleMusicButton: UIButton!
    
    @IBOutlet weak var itunesLibraryButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func cancel(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    //Adds a Song
    @IBAction func itunesLibrary(_ sender: AnyObject) {
        //Creates the song picker
        let picker = MPMediaPickerController.init(mediaTypes: MPMediaType.anyAudio)
        picker.delegate = self
        picker.allowsPickingMultipleItems = true
        picker.prompt = NSLocalizedString("Add songs to play", comment: "Prompt in media item picker")
        
        //Presents the song picker view
        self.present(picker, animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //media picker function to add music to queue
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        
        self.dismiss(animated: true, completion: {
            
            self.updateQueueWithCollection(mediaItemCollection)
            
        })
        
    }
    
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        self.dismiss(animated: true, completion: nil)
    }

    
    func updateQueueWithCollection(_ collection: MPMediaItemCollection) {
        //Updates Table Array With Picked Media Items
        for item in collection.items{
            
            let newAudioObject = AudioObjectClass()
            
            newAudioObject.mediaType = songType.ipod
            
            if let setArtist: NSString = item.artist as NSString? {
                newAudioObject.artist = setArtist
            }
            if let setAudioFile: MPMediaItem = item {
                newAudioObject.audioFile = setAudioFile
            }
            if let setImage: UIImage = item.artwork?.image(at: CGSize(width: 30, height: 30)){
                newAudioObject.image = setImage
            }
            newAudioObject.sender = "Me"
            if let setTitle: NSString = item.title as NSString? {
                newAudioObject.title = setTitle
            }
//            if let uID:UInt64 = item.persistentID {
//                let intID  = Int64(uID)
//                let numId = NSNumber(value: intID as Int64)
//                newAudioObject.id = numId
//            }
            
            audioObjects.append(newAudioObject)
        }
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
