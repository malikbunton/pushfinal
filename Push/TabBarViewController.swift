//
//  TabBarViewController.swift
//  Push
//
//  Created by Nashawn Chery on 1/10/18.
//  Copyright © 2018 Malik Bunton. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    
    fileprivate lazy var profileVC: UICollectionViewController = {
        let profileLayout = UICollectionViewFlowLayout()
        let profileVC = ProfileCollectionViewController(collectionViewLayout: profileLayout)
        return profileVC
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc1 = storyBoard.instantiateViewController(withIdentifier: "NowPlayingView") as! NowPlayingView
        
        let firstViewController = vc1
        
        firstViewController.tabBarItem = UITabBarItem(title: "Now Playing", image: #imageLiteral(resourceName: "Now Playing item"), tag: 0)
        
        let vc2 = storyBoard.instantiateViewController(withIdentifier: "QueueVC") as! QueueViewController
        let secondViewController = vc2
        
        secondViewController.tabBarItem = UITabBarItem(title: "Queue", image: #imageLiteral(resourceName: "queue tab item"), tag: 1)
        
        let profileLayout = UICollectionViewFlowLayout()
        let profileVC = ProfileCollectionViewController(collectionViewLayout: profileLayout)
        let thirdViewController = UINavigationController(rootViewController: profileVC)
        
        thirdViewController.tabBarItem = UITabBarItem(title: "Profile", image: #imageLiteral(resourceName: "Connectors tab item"), tag: 2)
        
        let tabBarList = [firstViewController, secondViewController, thirdViewController]
        
        viewControllers = tabBarList

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
