//
//  LoginViewController.swift
//  Push
//
//  Created by Ibrahim Conteh on 11/8/16.
//  Copyright © 2016 Malik Bunton. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    let emailField = UITextField.newAutoLayout()
    let passwordField = UITextField.newAutoLayout()
    let loginButton = UIButton(type: UIButtonType.roundedRect)
    let errorLabel = UILabel.newAutoLayout()

    override func viewDidLoad() {
        super.viewDidLoad()
        layout()
        
        emailField.delegate = self
        passwordField.delegate = self
        loginButton.addTarget(self, action: #selector(LoginViewController.logInButtonClicked), for: .touchUpInside)
        addNavBar()

        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        emailField.becomeFirstResponder()
        passwordField.becomeFirstResponder()
        // If the user is already authenticated, segue.
        if let _ = FIRAuth.auth()?.currentUser {

             let tabBar = TabBarViewController()
            self.present(tabBar, animated: true, completion: nil)
//
        }
    }
    
    override func viewDidLayoutSubviews(){
        emailField.setBottomBorder()
        passwordField.setBottomBorder()
    }
    
    func addNavBar(){
        let navBar: UINavigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 49))
        self.view.addSubview(navBar);
        let navItem = UINavigationItem();
        let doneItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: nil, action: #selector(self.backAction));
        navItem.leftBarButtonItem = doneItem;
        navItem.leftBarButtonItem?.tintColor = UIColor(red: 255/255.0, green: 120/255.0, blue: 0/255.0, alpha: 1)
        navBar.setItems([navItem], animated: false);
    }
    
    func backAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func logInButtonClicked(){
        //Add Input Validation
        if let email = emailField.text, let password = passwordField.text{
            FIRAuth.auth()?.signIn(withEmail: email, password: password, completion: { (user, error) in
                if error == nil {
                    let profileLayout = UICollectionViewFlowLayout()
                    let profileVC = ProfileCollectionViewController(collectionViewLayout: profileLayout)
                    self.present(profileVC, animated: true, completion: nil)
                }else{
                    self.handleFirebaseErrors(error: error! as NSError, onComplete: { (errMsg, data) in
                        self.addErrorLabel(message: errMsg!)
                    })
                }
            })
        }
    }

    
    func handleFirebaseErrors(error:NSError, onComplete: Completion){
        print(error.debugDescription)
        if let errorCode = FIRAuthErrorCode(rawValue: error.code){
            switch (errorCode){
            case .errorCodeInvalidEmail:
                onComplete("Invalid Email Address", nil)
                break
            case .errorCodeWrongPassword:
                onComplete("Wrong Password, Try Again", nil)
                break
            case .errorCodeUserDisabled:
                onComplete("User has been disabled, Please register again", nil)
                break
            case .errorCodeUserNotFound:
                onComplete("User not found, try again", nil)
            default:
                onComplete("There was a problem authenticating, Try Again", nil)
                break
            }
        }
        
    }
    
    func addErrorLabel(message: String){
        self.view.addSubview(errorLabel)
        errorLabel.textColor = UIColor.red
        errorLabel.autoAlignAxis(toSuperviewAxis: .vertical)
        errorLabel.autoPinEdge(.bottom, to: .bottom, of: passwordField, withOffset: 25)
        errorLabel.text = message
        errorLabel.font = UIFont.boldSystemFont(ofSize: 13)
        errorLabel.autoSetDimension(.height, toSize: 22)
        errorLabel.autoSetDimension(.width, toSize: view.frame.width - 50)
        errorLabel.center.x = self.view.center.x
        
    }

}

extension LoginViewController{
    func layout(){
      
        self.view.addSubview(emailField)
        self.view.addSubview(passwordField)
        self.view.addSubview(loginButton)
        emailField.autoSetDimension(.height, toSize: 45)
        emailField.autoSetDimension(.width, toSize: view.frame.width - 50)
        emailField.attributedPlaceholder = NSAttributedString(string: "EMAIL", attributes: [NSForegroundColorAttributeName: UIColor.gray])
        emailField.textColor = UIColor.white
        emailField.autoAlignAxis(toSuperviewAxis: .vertical)
        emailField.autoPinEdge(toSuperviewEdge: .top, withInset: 100)
        
        passwordField.autoSetDimension(.height, toSize: 45)
        passwordField.autoSetDimension(.width, toSize: view.frame.width - 50)
         passwordField.attributedPlaceholder = NSAttributedString(string: "PASSWORD", attributes: [NSForegroundColorAttributeName: UIColor.gray])
        passwordField.textColor = UIColor.white
        passwordField.autoAlignAxis(toSuperviewAxis: .vertical)
        passwordField.autoPinEdge(toSuperviewEdge: .top, withInset: 150)
        passwordField.isSecureTextEntry = true
        
        loginButton.autoSetDimension(.height, toSize: 45)
        loginButton.autoSetDimension(.width, toSize: view.frame.width - 50)
        loginButton.setTitle("Sign In", for: .normal)
        loginButton.setTitleColor(.white, for: .normal)
        loginButton.backgroundColor = UIColor(red: 255/255.0, green: 120/255.0, blue: 0/255.0, alpha: 1)
        loginButton.autoAlignAxis(toSuperviewAxis: .vertical)
        loginButton.autoPinEdge(toSuperviewEdge: .top, withInset: 225)
        
    }
}

extension UITextField {
    func setBottomBorder() {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
