//
//  PageViewController.swift
//  Push
//
//  Created by Nashawn Chery on 7/24/17.
//  Copyright © 2017 Malik Bunton. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController {
    
    let pageTitles = ["Host or Join", "Add your favorite music", "The Queue"]
    let pageDescriptions = ["Choose to host a playlist and control playback, or join an existing playlist and only contribute songs.", "Search and add your favorite music to the playlist.", "The Queue displays all songs added to the playlist."]
    let pageImages = ["image1", "image2", "image3"]
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataSource = self as UIPageViewControllerDataSource
        
        if let startTutorialVC = self.viewControllerAtIndex(index: 0){
            setViewControllers([startTutorialVC], direction: .forward, animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func viewControllerAtIndex (index:Int ) -> TutorialViewController?{
        if (index == NSNotFound || index < 0 || index >= self.pageDescriptions.count){
            return nil
        }
        
        if let tutorialViewController = storyboard?.instantiateViewController(withIdentifier: "TutorialViewController") as? TutorialViewController{
            tutorialViewController.imageName = pageImages[index]
            tutorialViewController.descriptionHeader = pageTitles[index]
            tutorialViewController.descriptionText = pageDescriptions[index]
            tutorialViewController.index = index
            
            return tutorialViewController
        }
        return nil
    }

}

extension PageViewController: UIPageViewControllerDataSource{
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! TutorialViewController).index
        index -= 1
        return self.viewControllerAtIndex(index: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! TutorialViewController).index
        index += 1
        return self.viewControllerAtIndex(index: index)
    }
}
