//
//  HomePageViewController.swift
//  Push
//
//  Created by Nashawn Chery on 1/9/18.
//  Copyright © 2018 Malik Bunton. All rights reserved.
//

import UIKit

class HomePageViewController: UINavigationController {
    let logo = UIImageView.newAutoLayout()
    let descriptionLabel = UILabel.newAutoLayout()
    let registerButton = UIButton(type: UIButtonType.roundedRect)
    let logInButton = UIButton(type: UIButtonType.roundedRect)
    let formDivider = FormDivider.newAutoLayout()
    let elementSize: CGSize = CGSize(width: 300, height: 45)
//    let registerView = RegisterViewController()

    override func viewDidLoad() {
        super.viewDidLoad()

        layout()
        // Do any additional setup after loading the view.
        registerButton.addTarget(self, action: #selector(HomePageViewController.registerButtonClicked), for: .touchUpInside)
        logInButton.addTarget(self, action: #selector(HomePageViewController.logInButtonClicked), for: .touchUpInside)
        let background = #imageLiteral(resourceName: "concertImage")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        imageView.layer.opacity = 0.3
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
        navigationBar.isHidden = true
//        self.view.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "concertImage"))
    }
    
    func registerButtonClicked(){
        let registerView = RegisterViewController()
//        let navController:UINavigationController = UINavigationController(rootViewController: registerView)
//        navController.navigationBar.barTintColor = UIColor.black
//        navController.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        self.present(registerView, animated: true, completion: nil)
    }
    
    func logInButtonClicked(){
        let logInView = LoginViewController()
        self.present(logInView, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomePageViewController {
    func layout(){
        view.addSubview(logo)
        view.addSubview(descriptionLabel)
        view.addSubview(registerButton)
        view.addSubview(formDivider)
        view.addSubview(logInButton)
        
        logo.image = #imageLiteral(resourceName: "Push official")
        logo.autoAlignAxis(toSuperviewAxis: .vertical)
        logo.autoSetDimensions(to: CGSize(width: 310, height: 340))
        logo.autoPinEdge(toSuperviewEdge: .top, withInset: 0)
        
        registerButton.backgroundColor = UIColor.black
        registerButton.setTitle("Register", for: .normal)
        registerButton.setTitleColor(.white, for: .normal)
        registerButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        registerButton.layer.borderWidth = 3
        registerButton.layer.borderColor = UIColor.white.cgColor
        registerButton.layer.cornerRadius = 5.0
        registerButton.layer.masksToBounds = false
        registerButton.autoSetDimensions(to: elementSize)
        registerButton.autoAlignAxis(toSuperviewAxis: .vertical)
        registerButton.autoPinEdge(toSuperviewEdge: .bottom, withInset: 60)
        
        
        logInButton.backgroundColor = UIColor.white
        logInButton.setTitle("Sign In", for: .normal)
        logInButton.setTitleColor(.black, for: .normal)
        logInButton.layer.borderWidth = 3
        logInButton.layer.borderColor = UIColor.black.cgColor
        logInButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        logInButton.layer.cornerRadius = 5.0
        logInButton.layer.masksToBounds = false
        logInButton.autoSetDimensions(to: elementSize)
        logInButton.autoAlignAxis(toSuperviewAxis: .vertical)
        logInButton.autoPinEdge(.bottom, to: .top, of: formDivider, withOffset: -10)
        
        formDivider.autoSetDimensions(to: elementSize)
        formDivider.autoAlignAxis(toSuperviewAxis: .vertical)
        formDivider.autoPinEdge(.bottom, to: .top, of: registerButton, withOffset: -10)
        
    }
}
