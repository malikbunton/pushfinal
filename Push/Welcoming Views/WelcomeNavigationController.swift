//
//  WelcomeNavigationController.swift
//  Push
//
//  Created by Nashawn Chery on 1/9/18.
//  Copyright © 2018 Malik Bunton. All rights reserved.
//

import UIKit

class WelcomeNavigationController: UINavigationController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    init() {
        let homePageController = HomePageViewController()
        super.init(rootViewController: homePageController)
    }

}
