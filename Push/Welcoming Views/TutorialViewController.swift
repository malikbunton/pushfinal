//
//  TutorialViewController.swift
//  Push
//
//  Created by Nashawn Chery on 7/23/17.
//  Copyright © 2017 Malik Bunton. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {

    @IBOutlet weak var viewImage: UIImageView!
    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var descriptionInfo: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var startButton: UIButton!
    
    var index = 0
    var descriptionHeader = ""
    var imageName = ""
    var descriptionText = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        descriptionTitle.text = descriptionHeader
        descriptionInfo.text = descriptionText
        viewImage.image = UIImage(named: imageName)
        pageControl.currentPage = index
        
        startButton.isHidden = (index == 3) ? true : false
                
    }
    
    @IBAction func startClicked(_ sender: Any) {
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(true, forKey: "DisplayedWalkThrough")
        self.dismiss(animated: true, completion: nil)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
