//
//  RegisterViewController.swift
//  Push
//
//  Created by Nashawn Chery on 1/7/18.
//  Copyright © 2018 Malik Bunton. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

typealias Completion = (_ errMsg: String?, _ data:AnyObject?) -> Void

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    var databaseRef: FIRDatabaseReference!
    var storageRef: FIRStorageReference!
    
    let emailField = UITextField.newAutoLayout()
    let usernameField = UITextField.newAutoLayout()
    let passwordField = UITextField.newAutoLayout()
    let errorLabel = UILabel.newAutoLayout()
    let registerButton = UIButton(type: UIButtonType.roundedRect)

    override func viewDidLoad() {
        super.viewDidLoad()
        layout()
        emailField.delegate = self
        passwordField.delegate = self
        navigationController?.isNavigationBarHidden = false
        registerButton.addTarget(self, action: #selector(RegisterViewController.registerButtonClicked), for: .touchUpInside)
        addBackButton()
        

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        emailField.becomeFirstResponder()
        usernameField.becomeFirstResponder()
        passwordField.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews(){
        emailField.setBottomBorder()
        usernameField.setBottomBorder()
        passwordField.setBottomBorder()
    }
    
    func addBackButton() {
        let navBar: UINavigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 49))
        self.view.addSubview(navBar);
//        let image = UIImageView.newAutoLayout()
//        image.image = #imageLiteral(resourceName: "launchpush")
//        image.autoSetDimensions(to: CGSize(width: 20, height: 100))
        let navItem = UINavigationItem();
        let doneItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: nil, action: #selector(self.backAction));
        navItem.leftBarButtonItem = doneItem;
        navItem.leftBarButtonItem?.tintColor = UIColor(red: 255/255.0, green: 120/255.0, blue: 0/255.0, alpha: 1)
//        navItem.titleView = image
        navBar.setItems([navItem], animated: false);
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "backBar"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.backAction))
//        self.navigationItem.leftBarButtonItem?.tintColor = UIColor(red: 255/255.0, green: 120/255.0, blue: 0/255.0, alpha: 1)
//        let image = UIImageView(image:#imageLiteral(resourceName: "LaunchIcon"))
//        image.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
//        self.navigationItem.titleView = image
        
    }
    
    func backAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func registerButtonClicked(){

        signUp(email: emailField.text, username: usernameField.text, password: passwordField.text, data: nil) { (errMsg, data) in
            guard errMsg == nil else{
                self.addErrorLabel(message: errMsg!)
                return
            }
        }
    }
    
    func signUp(email:String!, username:String!, password:String!, data:NSData!, onComplete: Completion?){
        FIRAuth.auth()?.createUser(withEmail: email, password: password, completion: { (user, error) in
            if error == nil{
                self.setUserInfo(user: user, username: username, password: password, data: nil)
            } else{
                self.handleFirebaseErrors(error: error! as NSError, onComplete: onComplete!)
            }
        })
    }
    
    func setUserInfo(user:FIRUser!, username:String, password:String, data:NSData!){
        if (data != nil){
            let imagePath = "profileImage\(user.uid)/userpic.jpg"
            
            self.storageRef = FIRStorage.storage().reference()
            let imageRef = storageRef.child(imagePath)
            
            let metaData = FIRStorageMetadata()
            metaData.contentType = "image/jpeg"
            
            imageRef.put(data as Data, metadata: metaData) { (metaData, error) in
                if error == nil{
                    let changeRequest = user.profileChangeRequest()
                    changeRequest.displayName = username
                    changeRequest.photoURL = metaData?.downloadURL()
                    changeRequest.commitChanges(completion: { (error) in
                        if error == nil{
                            self.saveInfo(user: user, username: username, password: password)
                        }else{
                            
                        }
                    })
                }else{
                    
                }
            }
        }else{
            let changeRequest = user.profileChangeRequest()
            changeRequest.displayName = username
            changeRequest.photoURL = nil
            changeRequest.commitChanges(completion: { (error) in
                if error == nil{
                    self.saveInfo(user: user, username: username, password: password)
                }else{
                    
                }
            })
        }
    }
    
    func saveInfo(user:FIRUser, username:String, password:String){
        let userInfo = ["email":user.email, "username":username, "userId":user.uid, "photoUrl":String(describing: user.photoURL)]
        
        self.databaseRef = FIRDatabase.database().reference()
        let userRef = databaseRef.child("users").child(user.uid).child("userInfo")
        
        userRef.setValue(userInfo)
    
        signIn(email: user.email!, password: password) { (errMsg, data) in
            
        }
    }
    
    func signIn(email: String, password: String, onComplete: Completion?){
        FIRAuth.auth()?.signIn(withEmail: email, password: password, completion: { (user, error) in
            if error == nil{
                let tabBar = TabBarViewController()
                self.present(tabBar, animated: true, completion: nil)
            } else{
                self.handleFirebaseErrors(error: error! as NSError, onComplete: onComplete!)
            }
        })
    }
    
    func handleFirebaseErrors(error:NSError, onComplete: Completion){
        print(error.debugDescription)
        if let errorCode = FIRAuthErrorCode(rawValue: error.code){
            switch (errorCode){
            case .errorCodeInvalidEmail:
                onComplete("Invalid Email Address", nil)
                break
            case .errorCodeWeakPassword:
                onComplete("Password Must Be at Least 6 Characters long", nil)
                break
            case .errorCodeEmailAlreadyInUse, .errorCodeAccountExistsWithDifferentCredential:
                onComplete("Email Already in Use", nil)
                break
            case .errorCodeCredentialAlreadyInUse:
                onComplete("Credentials Already in Use", nil)
                break
            default:
                onComplete("There was a problem authenticating, Try Again", nil)
                break
            }
        }
        
    }
    
    func addErrorLabel(message: String){
        self.view.addSubview(errorLabel)
        errorLabel.textColor = UIColor.red
        errorLabel.autoAlignAxis(toSuperviewAxis: .vertical)
        errorLabel.autoPinEdge(.bottom, to: .bottom, of: passwordField, withOffset: 25)
        errorLabel.text = message
        errorLabel.font = UIFont.boldSystemFont(ofSize: 13)
        errorLabel.autoSetDimension(.height, toSize: 22)
        errorLabel.autoSetDimension(.width, toSize: view.frame.width - 50)
        errorLabel.center.x = self.view.center.x
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RegisterViewController{
    func layout(){
        
        self.view.addSubview(emailField)
        self.view.addSubview(usernameField)
        self.view.addSubview(passwordField)
        self.view.addSubview(registerButton)
        
        
        emailField.borderStyle = .none
        emailField.autoSetDimension(.height, toSize: 45)
        emailField.autoSetDimension(.width, toSize: view.frame.width - 50)
        emailField.attributedPlaceholder = NSAttributedString(string: "EMAIL", attributes: [NSForegroundColorAttributeName: UIColor.gray])
        emailField.textColor = UIColor.white
        emailField.autoAlignAxis(toSuperviewAxis: .vertical)
        emailField.autoPinEdge(toSuperviewEdge: .top, withInset: 100)
        
        
        usernameField.borderStyle = .none
        usernameField.autoSetDimension(.height, toSize: 45)
        usernameField.autoSetDimension(.width, toSize: view.frame.width - 50)
        usernameField.attributedPlaceholder = NSAttributedString(string: "USERNAME", attributes: [NSForegroundColorAttributeName: UIColor.gray])
        usernameField.textColor = UIColor.white
        usernameField.autoAlignAxis(toSuperviewAxis: .vertical)
        usernameField.autoPinEdge(toSuperviewEdge: .top, withInset: 175)
        
        passwordField.borderStyle = .none
        passwordField.autoSetDimension(.height, toSize: 45)
        passwordField.autoSetDimension(.width, toSize: view.frame.width - 50)
        passwordField.attributedPlaceholder = NSAttributedString(string: "PASSWORD", attributes: [NSForegroundColorAttributeName: UIColor.gray])
        passwordField.textColor = UIColor.white
        passwordField.autoAlignAxis(toSuperviewAxis: .vertical)
        passwordField.autoPinEdge(toSuperviewEdge: .top, withInset: 250)
        passwordField.isSecureTextEntry = true
        
        
        registerButton.autoSetDimension(.height, toSize: 45)
        registerButton.autoSetDimension(.width, toSize: view.frame.width - 50)
        registerButton.setTitle("Register", for: .normal)
        registerButton.setTitleColor(.white, for: .normal)
        registerButton.backgroundColor = UIColor(red: 255/255.0, green: 120/255.0, blue: 0/255.0, alpha: 1)
        registerButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        registerButton.autoAlignAxis(toSuperviewAxis: .vertical)
        registerButton.autoPinEdge(toSuperviewEdge: .top, withInset: 350)
        
    }
    
}

